package com.profileTests;

import java.util.List;
import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.GenericUtilities.WaitHelper;
import com.PageObjects.objectsPOM;

public class LoginTest {

	WebDriver driver;
	public Logger log;
	objectsPOM objects=new objectsPOM();
	WaitHelper waithelper;
	Select sel;

@BeforeTest
public void LaunchBrowser() {
	System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
	driver=new ChromeDriver();
	driver.manage().window().maximize();
}

@BeforeClass
public void TypeUrl() throws InterruptedException {
	driver.get("http://www.seleniumeasy.com/test/basic-checkbox-demo.html");
	Thread.sleep(5000);
	log=Logger.getLogger(this.getClass().getName());
}

@Test
public void TC01_Dropdown() throws InterruptedException {

	System.out.println("entred in to test case clicked on input forms");
	driver.findElement(objects.inputforms).click();
	System.out.println("clicked on dropdown text");
	driver.findElement(objects.selectdropdowntext).click();
	System.out.println("selected all the options");
	sel=new Select(driver.findElement(objects.selectdropdown));
	sel.selectByIndex(1);
	Thread.sleep(2000);
	sel.selectByValue("Tuesday");
	Thread.sleep(2000);
	sel.selectByVisibleText("Thursday");
	Thread.sleep(2000);
}

@Test
public void TC02_MultiselectDropdown() throws InterruptedException {

	System.out.println("entred in to test case multi select one");
	WebElement multiselect=driver.findElement(objects.multiselectdropdown);
	sel=new Select(multiselect);
	sel.selectByIndex(1);
	Thread.sleep(2000);
	sel.selectByValue("New Jersey");
	Thread.sleep(2000);
	sel.selectByVisibleText("Texas");
	Thread.sleep(2000);
	sel.deselectByIndex(1);
	Thread.sleep(2000);
	sel.deselectByValue("New Jersey");
	Thread.sleep(2000);
	sel.deselectByVisibleText("Texas");
	
	List<WebElement> options = sel.getOptions();
	 for (WebElement option : options) {
		 System.out.println("All Options:--"+option.getText());
	 }
	 
	 System.out.println("Made Changes");
}

@AfterTest
public void quitbrowser() {
driver.quit();
}


}
