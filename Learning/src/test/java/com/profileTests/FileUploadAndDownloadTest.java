package com.profileTests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.GenericUtilities.BaseTest;
import com.GenericUtilities.GetData;
import com.PageObjects.objectsPOM;

public class FileUploadAndDownloadTest extends BaseTest {

objectsPOM objects=new objectsPOM();
GetData getdata=new GetData();


//################### http://toolsqa.com/automation-practice-form/ file upload and download ########

@Test
public void TC00_FileUpload() throws InterruptedException {

driver.get("http://toolsqa.com/automation-practice-form/");
driver.findElement(objects.FirstNameTextbox).sendKeys("Vinay");
Thread.sleep(3000);
driver.findElement(objects.LastNameTextbox).sendKeys("Lanka");

if (!driver.findElement(objects.RadiobuttonMale).isSelected()) {
	driver.findElement(objects.RadiobuttonMale).click();
}

Thread.sleep(3000);
if (!driver.findElement(objects.Radiobuttonexperience).isSelected()) {
	driver.findElement(objects.Radiobuttonexperience).click();
}

Thread.sleep(3000);
driver.findElement(objects.datepicker).sendKeys("10-07-2018");
Thread.sleep(3000);
driver.findElement(objects.AutomationTesterCheckbox).click();
Thread.sleep(3000);
driver.findElement(objects.UploadBrowsebutton).sendKeys("C:\\Backup_16022017\\My_Data\\Images\\Nature.jpg");
System.out.println("Upload succesful");

}

@Test
public void TC01_FileDownload() throws InterruptedException {

	
	String name = driver.findElement(By.linkText("Test File to Download.zip")).getText();
	driver.findElement(By.linkText("sample.zip")).click();
   //Task.Delay(5000).Wait();
    String path = "C:\\FlipkartReports";
    System.out.println("download succesful");
    
}

//################# Excel Sheet Comparison Selenium ########################

/*@Test
public void TC01_excel() throws IOException {
	
	
	ArrayList<String> one=new ArrayList<String>();
	ArrayList<String> two=new ArrayList<String>();
	
	for (int i=0;i<getdata.getRowCount("\\data\\ExampleSpace.xlsx","laptopspace");i++) {
		one.add(getdata.getDataFromExcel("\\data\\ExampleSpace.xlsx","laptopspace",i,0).replaceAll(" ",""));
		two.add(getdata.getDataFromExcel("\\data\\Example.xlsx","laptop",i,0));
	}
	
Collections.sort(one);
Collections.sort(two);

for (int j=0;j<one.size();j++) {

	if (one.get(j).equalsIgnoreCase(two.get(j))) {
		System.out.println("If condition in second for loop");
		getdata.setDataToExcel("\\data\\Compare.xlsx","comapre",j,0,one.get(j));
	}
	
}

System.out.println(one.equals(two));
	
}*/
	
}
