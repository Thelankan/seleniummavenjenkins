package com.profileTests;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.GenericUtilities.WaitHelper;
import com.PageObjects.objectsPOM;

public class LearningDropdowns {

	WebDriver driver;
	public Logger log;
	objectsPOM objects=new objectsPOM();
	WaitHelper waithelper;
	Select sel;

@BeforeTest
public void LaunchBrowser() {
	System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
	driver=new ChromeDriver();
	driver.manage().window().maximize();
}

@BeforeClass
public void TypeUrl() throws InterruptedException {
	//driver.get("http://www.seleniumeasy.com/test/basic-checkbox-demo.html");
	//driver.get("http://www.qaclickacademy.com");
	//driver.get("http://development-na01-campingworld.demandware.net/s/CampingWorld/60243.html");
	Thread.sleep(2000);
	log=Logger.getLogger(this.getClass().getName());
}


/*
//############## http://www.qaclickacademy.com #################
@Test
public void TC01_HadlingMultiplePopups() throws InterruptedException {
	
driver.get("http://www.qaclickacademy.com");	
System.out.println("before fetching size");
int footerlinks=driver.findElements(By.xpath("//div[@id='links']//a")).size();
System.out.println(footerlinks);

String ParentHandle=driver.getWindowHandle();

for (int i=0;i<footerlinks;i++) {
	
String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
driver.findElements(By.xpath("//div[@id='links']//a")).get(i).sendKeys(selectLinkOpeninNewTab);

//for (String handle1 :driver.getWindowHandles()) {
//	
//	  if (!ParentHandle.equals(handle1)) {
//		driver.switchTo().window(handle1);
//		Thread.sleep(4000);
//		System.out.println(driver.switchTo().window(handle1).getTitle());
//		Thread.sleep(4000);
//        driver.close();
//      }
	
	}
	
	driver.switchTo().window(ParentHandle);
	Thread.sleep(4000);
}

for(int i = driver.getWindowHandles().size() -1 ; i > 0 ; i--){

    String winHandle = driver.getWindowHandles().toArray()[i].toString();
    driver.switchTo().window(winHandle);
    Thread.sleep(2000);
    System.out.println(driver.switchTo().window(winHandle).getTitle());
    driver.close();
}
	
	
}


//############## http://www.seleniumeasy.com/test/basic-checkbox-demo.html #################
@Test
public void TC02_Dropdown() throws InterruptedException {

	driver.get("http://www.seleniumeasy.com/test/basic-checkbox-demo.html");
	driver.get("http://www.seleniumeasy.com/test/basic-checkbox-demo.html");
	System.out.println("entred in to test case clicked on input forms");
	driver.findElement(objects.inputforms).click();
	System.out.println("clicked on dropdown text");
	driver.findElement(objects.selectdropdowntext).click();
	System.out.println("selected all the options");
	sel=new Select(driver.findElement(objects.selectdropdown));
	sel.selectByIndex(1);
	Thread.sleep(2000);
	sel.selectByValue("Tuesday");
	Thread.sleep(2000);
	sel.selectByVisibleText("Thursday");
	Thread.sleep(2000);
}

@Test
public void TC03_MultiselectDropdown() throws InterruptedException {

	driver.get("http://www.seleniumeasy.com/test/basic-checkbox-demo.html");
	System.out.println("entred in to test case multi select one");
	WebElement multiselect=driver.findElement(objects.multiselectdropdown);
	sel=new Select(multiselect);
	sel.selectByIndex(1);
	Thread.sleep(2000);
	sel.selectByValue("New Jersey");
	Thread.sleep(2000);
	sel.selectByVisibleText("Texas");
	Thread.sleep(2000);
	sel.deselectByIndex(1);
	Thread.sleep(2000);
	sel.deselectByValue("New Jersey");
	Thread.sleep(2000);
	sel.deselectByVisibleText("Texas");
	
	List<WebElement> options = sel.getOptions();
	 for (WebElement option : options) {
		 System.out.println("All Options:--"+option.getText());
	 }
}

@Test
public void TC04_jquerydropdown() throws InterruptedException {

	driver.get("http://www.seleniumeasy.com/test/basic-checkbox-demo.html");
	driver.findElement(objects.inputforms).click();
	System.out.println("clicked on dropdown text");
	driver.findElement(objects.JqueryDropdown).click();
	Thread.sleep(2000);
	sel=new Select(driver.findElement(objects.SelectCountry));
	Thread.sleep(4000);
	sel.selectByIndex(1);
	Thread.sleep(4000);
	sel=new Select(driver.findElement(objects.JqueryMultiselectDropdown));
	Thread.sleep(4000);
	sel.selectByIndex(2);
	Thread.sleep(2000);
	sel.selectByIndex(3);
	Thread.sleep(4000);
	sel=new Select(driver.findElement(objects.JqueryMultiCategoryDropdown));
	sel.selectByVisibleText("Python");
	Thread.sleep(4000);
	sel.selectByVisibleText("Java");
	
	try {
	driver.findElements(By.xpath("//li[@class='tree-branch']")).get(0).getText();
	} catch (Exception e) {
		System.out.println("null elements return");
		e.printStackTrace();
	}
	
	try {
		driver.findElement(By.xpath("objects.SelectCountry"));
	} catch (Exception e) {
		System.out.println("null elements return");
		e.printStackTrace();
	}
	
}

@Test
public void TC04_Returntypeofwebelementandelements() throws InterruptedException {

// #################### Find Web element and web elements differences return types ###############

driver.findElement(By.xpath("//a[contains(text(),'Selenium Easy')]")).click();
driver.findElements(By.xpath("//li[@class='tree-branch']")).get(0).click();


//@@@@@@@@@@@@@@@@@@@@@ email get attribute from email text box @@@@@@@@@@@@@@@@@@@@@@@@@@@	
driver.findElement(By.xpath("//span[contains(text(),'Login')]")).click();
driver.findElement(By.xpath("//input[@id='user_email']")).sendKeys("gmail@gmail.com");
String emailvalue=driver.findElement(By.xpath("//input[@id='user_email']")).getAttribute("value");
System.out.println("Email Value"+emailvalue);
	
}

//#################### https://www.makemytrip.com ################

public void iFrame() {
	
	driver.get("https://www.makemytrip.com/");
	driver.switchTo().frame("webklipper-publisher-widget-container-notification-frame");
	driver.findElement(objects.closefile).click();
	System.out.println("escape executed");
}
*/

//############## http://www.qaclickacademy.com #################
@Test
public void TC01_HadlingMultiplePopups() throws InterruptedException {


for(int i=0;i<10;i++) {
	
driver.get("http://google.com");
String windowHandle = driver.getWindowHandle();
driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");
	
//Check the size of the output of getWindowHandles().

ArrayList tabs = new ArrayList (driver.getWindowHandles());
System.out.println(tabs.size());
driver.switchTo().window((String) tabs.get(i));
		    
//The control is now in the new tab-

driver.get("http://www.qaclickacademy.com");

//perform other operations on new tab.
//Switch to the old tab using Ctrl + Tab:

//		    driver.switchTo().window(mainWindowHandle);
//		    driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"\t");
//		    driver.switchTo().defaultContent();
		    
	
}

}

@AfterTest
public void quitbrowser() {
driver.quit();
}


}
