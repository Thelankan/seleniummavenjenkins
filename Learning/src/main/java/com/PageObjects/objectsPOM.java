package com.PageObjects;

import org.openqa.selenium.By;

public class objectsPOM {
	
	public By inputforms=By.xpath("//ul[@id='treemenu']//a[contains(text(),'Input Forms')]");
	public By selectdropdowntext=By.xpath("//ul[@id='treemenu']//a[contains(text(),'Select Dropdown List')]");
	public By selectdropdown=By.xpath("//select[@id='select-demo']");
	public By multiselectdropdown=By.xpath("//select[@id='multi-select']");
	public By SelectCountry=By.xpath("//select[@id='country']");
	public By JqueryMultiselectDropdown=By.xpath("//select[@class='js-example-basic-multiple select2-hidden-accessible']");
	public By JqueryMultiCategoryDropdown=By.xpath("//select[@id='files']");
	public By JqueryDropdown=By.xpath("//ul[@id='treemenu']//a[contains(text(),'JQuery Select dropdown')]");
	
	public By FirstNameTextbox=By.xpath("//input[@name='firstname']");
	public By LastNameTextbox=By.xpath("//input[@name='lastname']");
	public By RadiobuttonMale=By.xpath("//input[@id='sex-0']");       
	public By Radiobuttonexperience=By.xpath("//input[@id='exp-3']");
	public By datepicker=By.xpath("//input[@id='datepicker']");
	public By AutomationTesterCheckbox=By.xpath("//input[@id='profession-1']");
	public By UploadBrowsebutton=By.xpath("//input[@id='photo']");
	
	
	public By closefile=By.xpath("//div[@id='webklipper-publisher-widget-container-notification-container']//a[@id='webklipper-publisher-widget-container-notification-close-div']");
	
	

}
