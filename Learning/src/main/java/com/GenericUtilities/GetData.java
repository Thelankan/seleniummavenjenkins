package com.GenericUtilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class GetData {

String data;	
File f;

public String getDataFromExcel(String filePath,String sheetname,int rownumber,int cellnumber) throws IOException
{
	f=new File(System.getProperty("user.dir")+filePath);
	FileInputStream fis=new FileInputStream(f);
	@SuppressWarnings("resource")
	Workbook wb=new XSSFWorkbook(fis);
	Sheet sheet=wb.getSheet(sheetname);
	Row row=sheet.getRow(rownumber);
	data=row.getCell(cellnumber).toString();
	return data;
}

public void setDataToExcel(String filePath,String sheetname,int rownumber,int coloumnnumber,String value) throws IOException
{
	f=new File(System.getProperty("user.dir")+filePath);
	FileInputStream fis=new FileInputStream(f);
	Workbook wbo=new XSSFWorkbook(fis);
	Sheet sheet=wbo.getSheet(sheetname);
	
	Row row = null;
	Cell c = null;
	
	try {
		System.out.println("entred try");
		row=sheet.getRow(rownumber);
		c=row.getCell(coloumnnumber);
		System.out.println("xssf getrow no and cell no");
		
	} catch (Exception e) {
		
		row = sheet.createRow(rownumber);
	}
	
	row.createCell(coloumnnumber).setCellValue(value);
	
	FileOutputStream fos=new FileOutputStream(f);
	wbo.write(fos);
	
}


public int getRowCount(String filePath,String sheetName) throws IOException{
	  
	f=new File(System.getProperty("user.dir")+filePath);
	FileInputStream fis=new FileInputStream(f);
	Workbook wb=new XSSFWorkbook(fis);
	Sheet sh=wb.getSheet(sheetName);
	int rowCount = sh.getLastRowNum()+1;
	return rowCount;
  
 }

public int getColumnCount(String filePath,String sheetName,int RowNo) throws IOException{
	  
		f=new File(System.getProperty("user.dir")+filePath);
		FileInputStream fis=new FileInputStream(f);
		Workbook wb=new XSSFWorkbook(fis);
		Sheet sh=wb.getSheet(sheetName);
		Row row = sh.getRow(RowNo);
		int colCount = row.getLastCellNum();
		return colCount;
     
 }






}
