package com.GenericUtilities;

import java.util.List;
import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.GenericUtilities.WaitHelper;
import com.PageObjects.objectsPOM;

public class BaseTest {

	public WebDriver driver;
	public Logger log;
	objectsPOM objects=new objectsPOM();
	WaitHelper waithelper;
	Select sel;

@BeforeTest
public void LaunchBrowser() {
	System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
	driver=new ChromeDriver();
	driver.manage().window().maximize();
}

@BeforeClass
public void TypeUrl() throws InterruptedException {
	//driver.get("http://toolsqa.com/automation-practice-form/");
	//driver.navigate().to("https://www.naukri.com/");
	//driver.get("https://www.makemytrip.com/");//Iframe
	//driver.get("https://www.quora.com/How-many-companies-are-there-in-Bangalore");
	Thread.sleep(5000);
	log=Logger.getLogger(this.getClass().getName());
}

@AfterTest
public void quitbrowser() {
//driver.quit();
}
}
